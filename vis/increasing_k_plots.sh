#!/bin/bash
path='../data/Data_for_Mann_et_al_RSBL 2/Bladon & Church route recapping/bladon heath/brc'
l='10'

for k in {2..5}; do
	./clustering "$path" "$k" "$l" && ../vis/./plot_clustering.py out.clustering && mv out.clustering.pdf "out_${k}.pdf"
done
