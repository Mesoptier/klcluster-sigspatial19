#pragma once

#include "curve.h"

Curve simplify(Curve const& curve, int l);
